import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 骑手api接口集合
 * @method riderList 骑手列表
 * @method stoaudit 审核接口
 * @method stocancel 骑手删除
 * @method recordList 骑手接单记录
 */
export function riderApi() {
	return {
		riderList: () => {
			return request({
				url: '/spigall/Rider',
				method: 'post',
			});
		},
		stoaudit: (data: any) => {
			return request({
				url: '/spigall/Audit',
				method: 'post',
				data,
			});
		},
		stoRider: (data: any) => {
			return request({
				url: '/spigall/Rider/dele',
				method: 'post',
				data,
			});
		},
		recordList: (data: any) => {
			return request({
				url: '/spigall/Rider/record',
				method: 'post',
				data,
			});
		},
	};
}
