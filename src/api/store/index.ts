import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 店铺api接口集合
 * @method storeList 店铺列表
 * @method storeShow 店铺上下架
 * @method stoaudit 审核接口
 * @method stoshelf 店铺快速上架
 * @method stocancel 店铺删除
 * @method storetype 店铺类型列表
 * @method storetypeadd 店铺类型列表
 * @method storetypedele 店铺类型删除
 * @method serceList     店铺服务列表
 * @method serceShow 店铺服务上下架
 * @method sercehelf 店铺服务快速上架
 * @method serceancel 店铺服务删除
 */
export function storeApi() {
	return {
		storeList: () => {
			return request({
				url: '/spigall/store',
				method: 'post',
			});
		},
		storeShow: (data: any) => {
			return request({
				url: '/spigall/store/speedines',
				method: 'post',
				data,
			});
		},
		stoaudit: (data: any) => {
			return request({
				url: '/spigall/Audit',
				method: 'post',
				data,
			});
		},
		stoshelf: (data: any) => {
			return request({
				url: '/spigall/store/speediness',
				method: 'post',
				data,
			});
		},
		stocancel: (data: any) => {
			return request({
				url: '/spigall/store/dele',
				method: 'post',
				data,
			});
		},
		storetype: () => {
			return request({
				url: '/spigall/storetype',
				method: 'post',
			});
		},
		storetypeadd: (data: any) => {
			return request({
				url: '/spigall/storetype/add',
				method: 'post',
				data,
			});
		},
		storetypedele: (data: any) => {
			return request({
				url: '/spigall/storetype/dele',
				method: 'post',
				data,
			});
		},
		serceList: (data: any) => {
			return request({
				url: '/spigall/Service',
				method: 'post',
				data,
			});
		},
		serceShow: (data: any) => {
			return request({
				url: '/spigall/Service/speedines',
				method: 'post',
				data,
			});
		},
		sercehelf: (data: any) => {
			return request({
				url: '/spigall/Service/speediness',
				method: 'post',
				data,
			});
		},
		serceancel: (data: any) => {
			return request({
				url: '/spigall/Service/dele',
				method: 'post',
				data,
			});
		},
	};
}
