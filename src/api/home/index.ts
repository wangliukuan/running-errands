import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 数据统计接口集合
 * @method atistics 数据统计
 */
export function useHomeApi() {
	return {
		atistics: () => {
			return request({
				url: '/spigall/Statis',
				method: 'post',
			});
		},
	};
}
