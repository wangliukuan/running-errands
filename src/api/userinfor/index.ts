import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 数据统计接口集合
 * @method person 个人信息
 */
export function userinfoApi() {
	return {
		person: (data: any) => {
			return request({
				url: '/spigall/amend',
				method: 'post',
				data,
			});
		},
		personupda: (data: any) => {
			return request({
				url: '/spigall/amend/upda',
				method: 'post',
				data,
			});
		},
	};
}
