import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 骑手api接口集合
 * @method comborderlist 求助列表
 * @method Helplist 求助列表
 */
export function comborderApi() {
	return {
		comborderlist: (data: any) => {
			return request({
				url: '/spigall/Order',
				method: 'post',
				data,
			});
		},
		rapidRacking: (data: object) => {
			return request({
				url: '/spigall/sekhlp/speedines',
				method: 'post',
				data,
			});
		},
		rapidRackings: (data: object) => {
			return request({
				url: '/spigall/sekhlp/speediness',
				method: 'post',
				data,
			});
		},
		deleteHelp: (data: object) => {
			return request({
				url: '/spigall/Order/dele',
				method: 'post',
				data,
			});
		},
	};
}
