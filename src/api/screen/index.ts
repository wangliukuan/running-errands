import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 店铺api接口集合
 * @method screenList 广告列表
 * @method screenShow 广告上下架
 * @method screenshelf 广告快速上架
 * @method screencancel 广告删除
 */
export function screenApi() {
	return {
		screenList: () => {
			return request({
				url: '/spigall/screen',
				method: 'post',
			});
		},
		screenShow: (data: any) => {
			return request({
				url: '/spigall/screen/speedines',
				method: 'post',
				data,
			});
		},
		screenshelf: (data: any) => {
			return request({
				url: '/spigall/screen/speediness',
				method: 'post',
				data,
			});
		},
		screencancel: (data: any) => {
			return request({
				url: '/spigall/screen/dele',
				method: 'post',
				data,
			});
		},
	};
}
