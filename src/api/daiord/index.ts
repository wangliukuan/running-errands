import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 套餐api接口集合
 * @method daiordList 列表
 */
export function daiordApi() {
	return {
		daiordList: () => {
			return request({
				url: '/spigall/Daiord',
				method: 'post',
			});
		},
	};
}
