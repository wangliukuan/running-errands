import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 套餐api接口集合
 * @method comboList 套餐列表
 * @method comboAdd 添加接口
 * @method depositdel 删除
 * @method comboShow 快速上下架
 */
export function comboApi() {
	return {
		comboList: (data: any) => {
			return request({
				url: '/spigall/Semeal',
				method: 'post',
				data,
			});
		},
		comboAdd: (data: any) => {
			return request({
				url: '/spigall/Semeal/add',
				method: 'post',
				data,
			});
		},
		combodel: (data: any) => {
			return request({
				url: '/spigall/Semeal/dele',
				method: 'post',
				data,
			});
		},
		comboShow: (data: any) => {
			return request({
				url: '/spigall/Semeal/speedines',
				method: 'post',
				data,
			});
		},
	};
}
