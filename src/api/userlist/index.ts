import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 用户api接口集合
 * @method userList 用户列表
 * @method userRecord 用户接单记录
 */
export function userApi() {
	return {
		userList: () => {
			return request({
				url: '/spigall/Userlist',
				method: 'post',
			});
		},
		userRecord: (data: any) => {
			return request({
				url: '/spigall/Userlist/list',
				method: 'post',
				data,
			});
		},
	};
}
