import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 提现api接口集合
 * @method comboList 提现列表
 * @method stoaudit 审核接口
 * @method depositdel 删除
 */
export function comboApi() {
	return {
		comboList: (data: any) => {
			return request({
				url: '/spigall/Semeal',
				method: 'post',
				data,
			});
		},
		stoaudit: (data: any) => {
			return request({
				url: '/spigall/Audit',
				method: 'post',
				data,
			});
		},
		depositdel: (data: any) => {
			return request({
				url: '/spigall/Deposit/dele',
				method: 'post',
				data,
			});
		},
	};
}
