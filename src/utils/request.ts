import axios, { AxiosInstance } from 'axios';
import { ElMessage, ElMessageBox } from 'element-plus';
import { Session } from '/@/utils/storage';
import qs from 'qs';
import { loginTOKEN } from '/@/stores/logins';
import pinia from '/@/stores/index';
const stores = loginTOKEN(pinia);

// 配置新建一个 axios 实例
const service: AxiosInstance = axios.create({
	baseURL: import.meta.env.VITE_API_URL,
	timeout: 50000,
	headers: {
		token: Session.get('token'),
		id: Session.get('id') ? Session.get('token') : '',
	},
	paramsSerializer: {
		serialize(params) {
			return qs.stringify(params, { allowDots: true });
		},
	},
});

// 添加请求拦截器
service.interceptors.request.use(
	(config) => {
		// 在发送请求之前做些什么 token
		if (Session.get('token')) {
			config.headers!['token'] = `${Session.get('token')}`;
			config.headers!['id'] = `${Session.get('id')}`;
			return config;
		} else {
			config.headers!['token'] = stores.logtoken;
			config.headers!['id'] = stores.logid;
		}
		return config;
	},
	(error) => {
		// 对请求错误做些什么
		return Promise.reject(error);
	}
);

// 添加响应拦截器
service.interceptors.response.use(
	(response) => {
		// 对响应数据做点什么
		let res = { ...response.data };
		if (res.code == 0) {
			// `token` 过期或者账号已在别处登录
			if (res.msg == '权限错误') {
				ElMessageBox.alert('你已被登出，请重新登录', '提示', {})
					.then(() => {
						Session.clear(); // 清除浏览器全部临时缓存
						localStorage.clear();
						window.location.href = '/'; // 去登录页
					})
					.catch(() => {
						Session.clear(); // 清除浏览器全部临时缓存
						localStorage.clear();
						window.location.href = '/'; // 去登录页
					});
				return Promise.reject(service.interceptors.response);
			}
		} else {
			return res.data;
		}
	},
	(error) => {
		// 对响应错误做点什么
		if (error.message.indexOf('timeout') != -1) {
			ElMessage.error('网络超时');
		} else if (error.message == 'Network Error') {
			ElMessage.error('网络连接错误');
		} else {
			if (error.response.data) ElMessage.error(error.response.statusText);
			else ElMessage.error('接口路径找不到');
		}
		return Promise.reject(error);
	}
);

// 导出 axios 实例
export default service;
