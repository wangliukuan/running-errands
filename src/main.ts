import { createApp } from 'vue';
import pinia from '/@/stores/index';
import App from '/@/App.vue';
import router from '/@/router';
import { directive } from '/@/directive/index';
import { i18n } from '/@/i18n/index';
import other from '/@/utils/other';

import ElementPlus from 'element-plus';
import zhCn from 'element-plus/dist/locale/zh-cn.mjs';
import '/@/theme/index.scss';
import VueGridLayout from 'vue-grid-layout';
// 在 main.js 中引入
import '/@/fontFamily/font.css';
import piniaPersist from 'pinia-plugin-persist';
import VueUeditorWrap from 'vue-ueditor-wrap';
const app = createApp(App);
directive(app);
other.elSvg(app);
pinia.use(piniaPersist);

app
	.use(pinia)
	.use(router)
	.use(ElementPlus, {
		locale: zhCn,
	})
	.use(i18n)
	.use(VueGridLayout)
	.use(VueUeditorWrap)
	.mount('#app');
