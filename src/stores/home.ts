// https://pinia.vuejs.org/
import { defineStore } from 'pinia';
/**
 * 数据统计api
 * @methods sethomeIndex        直视区域
 * @methods setorderData        订单漏斗
 * @methods setorderDatas       订单饼图
 * @methods setpieData          分布饼图
 * @methods setcolumnarData     用户消费排行榜
 * @methods sethistogramData    骑手收益排行榜
 * @methods setcheckinData      店铺申请列表
 *
 */

export const homeIndex = defineStore('homeIndex', {
	state: (): homeIndexState => ({
		homeIndex: [],
		orderData: [],
		orderDatas: [],
		pieData: [],
		columnarData: [],
		histogramData: [],
		checkinData: [],
		checkinsData: [],
	}),
	actions: {
		async sethomeIndex(data: Array<any>) {
			this.homeIndex = data;
		},
		async setorderData(data: Array<any>) {
			this.orderData = data;
		},
		async setorderDatas(data: Array<any>) {
			this.orderDatas = data;
		},
		async setpieData(data: Array<any>) {
			this.pieData = data;
		},
		async setcolumnarData(data: Array<any>) {
			this.columnarData = data;
		},
		async sethistogramData(data: Array<any>) {
			this.histogramData = data;
		},
		async setcheckinData(data: Array<any>) {
			this.checkinData = data;
		},
		async setcheckinsData(data: Array<any>) {
			this.checkinsData = data;
		},
	},
	persist: {
		enabled: true,
		strategies: [
			{
				key: 'homeIndex',
				storage: localStorage,
				paths: ['homeIndex'],
			},
			{
				key: 'orderData',
				storage: localStorage,
				paths: ['orderData'],
			},
			{
				key: 'orderDatas',
				storage: localStorage,
				paths: ['orderDatas'],
			},
			{
				key: 'pieData',
				storage: localStorage,
				paths: ['pieData'],
			},
			{
				key: 'columnarData',
				storage: localStorage,
				paths: ['columnarData'],
			},
			{
				key: 'histogramData',
				storage: localStorage,
				paths: ['histogramData'],
			},
			{
				key: 'checkinData',
				storage: localStorage,
				paths: ['checkinData'],
			},
			{
				key: 'checkinsData',
				storage: localStorage,
				paths: ['checkinsData'],
			},
		],
	},
});
