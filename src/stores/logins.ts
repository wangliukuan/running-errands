// https://pinia.vuejs.org/
import { defineStore } from 'pinia';
/**
 * 店铺分类api
 * @methods loginToken       token
 *
 */

export const loginTOKEN = defineStore('loginTOKEN ', {
	state: (): loginT => ({
		logtoken: {},
		logid: {},
	}),
	actions: {
		async loginToken(data: any) {
			this.logtoken = data;
		},
		async loginId(data: any) {
			this.logid = data;
		},
	},
});
