// https://pinia.vuejs.org/
import { defineStore } from 'pinia';
/**
 * 店铺分类api
 * @methods userList        用户信息
 *
 */

export const userines = defineStore('userines ', {
	state: (): userList => ({
		userin: {},
	}),
	actions: {
		async userList(data: any) {
			this.userin = data;
		},
	},
});
