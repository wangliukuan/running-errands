// https://pinia.vuejs.org/
import { defineStore } from 'pinia';
/**
 * 店铺分类api
 * @methods setriderList        骑手列表
 *
 */

export const riderList = defineStore('riderList ', {
	state: (): storeRiderStore => ({
		setrider: {},
	}),
	actions: {
		async setriderList(data: any) {
			this.setrider = data;
		},
	},
	persist: {
		enabled: true,
		strategies: [
			{
				key: 'setrider',
				storage: localStorage,
				paths: ['setrider'],
			},
		],
	},
});
