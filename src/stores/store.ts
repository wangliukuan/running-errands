// https://pinia.vuejs.org/
import { defineStore } from 'pinia';
/**
 * 店铺分类api
 * @methods setstoreClif        店铺列表
 * @methods setstoreList        店铺分类
 *
 */

export const storeClif = defineStore('storeClif ', {
	state: (): storeClifState => ({
		storeClifs: [],
		storeList: [],
	}),
	actions: {
		async setstoreClif(data: any) {
			this.storeClifs = data;
		},
		async setstoreList(data: any) {
			this.storeList = data;
		},
	},
	persist: {
		enabled: true,
		strategies: [
			{
				key: 'storeClifs',
				storage: localStorage,
				paths: ['storeClifs'],
			},
			{
				key: 'storeList',
				storage: localStorage,
				paths: ['storeList'],
			},
		],
	},
});
